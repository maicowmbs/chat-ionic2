
import { Injectable } from '@angular/core';
import { AngularFireDatabaseModule, AngularFireDatabase } from 'angularfire2/database';
import { AngularFireAuth } from 'angularfire2/auth';
// import { Router } from "@angular/router";
import * as firebase from 'firebase';
import { BaseService } from '../base/base.service';

@Injectable()
export class AuthService extends BaseService{

  authState: any = null;

  constructor(private afAuth: AngularFireAuth,
              private db: AngularFireDatabase,) {
                super();
                this.afAuth.authState.subscribe((auth) => {
                          this.authState = auth
                        });;
  }
  createAuthUser(user: {email: string, password: string }) {
      return this.afAuth.auth.createUserWithEmailAndPassword(user.email, user.password).catch(this.handlePromiseError);
              // .then((user) => {
              //   this.authState = user
              //   // this.updateUserData()
              // })
              // .catch(error => console.log(error));
  }
  //   private updateUserData(): void {
  // // Writes user name and email to realtime db
  // // useful if your app displays information about users or for admin features
  //   let path = `users/${this.currentUserId}`; // Endpoint on firebase
  //   let data = {
  //                 email: this.authState.email,
  //                 name: this.authState.displayName
  //               }

  //   this.db.object(path).update(data)
  //   .catch(error => console.log(error));

  // }
}
