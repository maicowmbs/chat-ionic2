import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';

import { BaseService } from '../base/base.service';
import * as firebase from 'firebase';

import {
  AngularFireDatabase
} from "angularfire2/database";
import { User } from "../../models/user.model";
import { Observable } from "rxjs/Observable";

@Injectable()
export class UserService extends BaseService {
  public users: Observable<User[]>;

  constructor(public af: AngularFireDatabase, public http: Http) {
    super();
    this.users = af.list(`/users`).valueChanges();
    console.log(af.list<User>(`/users`).valueChanges());
  }



  create(user: User): firebase.Promise<void> {
    return this.af.object(`/users/${user.uid}`).set(user).catch(this.handlePromiseError);
  }

  userExists(username: string): Observable<boolean> {
    this.af.list(`/users`, 
      ref => ref.orderByChild('username').equalTo(username)
    ).valueChanges()
    .map((users: User[]) => {
      return users.length > 0;
    });

    return null;
  }

}
