import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';
import { HttpModule } from '@angular/http';

import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';
import { SignupPage } from '../pages/signup/signup';

import { AngularFireModule, FirebaseAppConfig } from 'angularfire2';
import { AngularFireDatabaseModule } from 'angularfire2/database';
import { AngularFireAuthModule } from 'angularfire2/auth';

import { BaseService } from '../providers/base/base.service';
import { UserService } from "../providers/user/user.service";

import * as firebase from 'firebase/app';
import { AuthService } from '../providers/auth/auth.service';

const firebaseAppConfig: FirebaseAppConfig = {
    apiKey: "AIzaSyA57X_d1nMWxxrrfycYLg_7BlAhd-eyNHs",
    authDomain: "ion2-chat.firebaseapp.com",
    databaseURL: "https://ion2-chat.firebaseio.com",
    projectId: "ion2-chat",
    storageBucket: "ion2-chat.appspot.com",
    messagingSenderId: "796716745787"
  };

@NgModule({
  declarations: [
    MyApp,
    HomePage,
    SignupPage
  ],
  imports: [
    BrowserModule,
    HttpModule,
    IonicModule.forRoot(MyApp),
    AngularFireModule.initializeApp(firebaseAppConfig),
    AngularFireDatabaseModule,
    AngularFireAuthModule
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    SignupPage
  ],
  providers: [
    
    StatusBar,
    SplashScreen,
    UserService,
    AuthService,

    {provide: ErrorHandler, useClass: IonicErrorHandler}
    
  ]
})
export class AppModule {}
