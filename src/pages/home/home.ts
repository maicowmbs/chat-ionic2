import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { SignupPage } from '../signup/signup';
import { AngularFireList } from "angularfire2/database";
import { User } from '../../models/user.model';
import { UserService } from '../../providers/user/user.service';
import { Observable } from 'rxjs/Observable';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  users: Observable<User[]>;

  constructor(public navCtrl: NavController, public userService: UserService) {

  }

  ionViewDidLoad(){
    this.users = this.userService.users;
    // console.log(this.users);
    
  }
  onChatCreate(user: User): void{
    console.log(user);
    
  }

  onSignup(): void {
    this.navCtrl.push(SignupPage)
  }

}
