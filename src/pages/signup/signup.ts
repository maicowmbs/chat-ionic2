import { Component } from '@angular/core';
import {
  IonicPage,
  NavController,
  NavParams,
  LoadingController,
  Loading,
AlertController
} from "ionic-angular";
import { FormBuilder, Validators, FormGroup } from "@angular/forms";
import 'rxjs/add/operator/first';
import { UserService } from '../../providers/user/user.service';
import { AuthService } from '../../providers/auth/auth.service';
import { User } from '../../models/user.model';


import * as firebase from 'firebase/app';
import { Observable } from 'rxjs/Observable';


@Component({
  selector: 'page-signup',
  templateUrl: 'signup.html',
})
export class SignupPage {

  signupForm: FormGroup;

  constructor(
    public authService: AuthService,
    public alertCtrl: AlertController,
    public formBuilder: FormBuilder,
    public loadingCtrl: LoadingController,
    public navCtrl: NavController, 
    public navParams: NavParams,
    public userService: UserService
    ) {

    let emailRegex = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

    this.signupForm = this.formBuilder.group(
      {
        name: ['', [Validators.required, Validators.minLength(3)]],
        username: ['', [Validators.required, Validators.minLength(3)]],
        email: ['', Validators.compose([Validators.required, Validators.pattern(emailRegex)])],
        password: ['', [Validators.required, Validators.minLength(6)]],
      }
    )

  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad SignupPage');
  }

  onSubmit(): void {

    let loading: Loading = this.showLoading();

    let formUser = this.signupForm.value;

    let username: string = formUser.username;

    this.userService.userExists(username)
      .first()
      .subscribe((userExists: boolean) => {

        if(!userExists){


                this.authService.createAuthUser({
                  email: formUser.email,
                  password: formUser.password
                }).then((authState: any) => {

                  delete formUser.password;
                  formUser.uid = authState.uid;

                  this.userService.create(formUser)
                    .then(() => {
                      console.log('usuario criado');
                      loading.dismiss();
                    }).catch((error: any) => {
                        console.log(error);
                        loading.dismiss();
                        this.showAlert(error);
                      });

                }).catch((error: any) => {
                  console.log(error);
                  loading.dismiss();
                  this.showAlert(error);
                });


        } else {
          this.showAlert(`O username ${username} já está sendo utilizado!`);
          loading.dismiss();
        }

      })


  }

  private showLoading(): Loading {
    let loading: Loading = this.loadingCtrl.create({
      content: 'Please wait...'
    });

    loading.present();

    return loading;
  }

  private showAlert(message: string): void {
    this.alertCtrl.create({
      message: message,
      buttons:['OK']
    }).present();
  }

}
